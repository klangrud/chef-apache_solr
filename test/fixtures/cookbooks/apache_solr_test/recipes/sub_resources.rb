#
# Cookbook Name:: apache_solr_test
# Recipe:: custom
#
# Copyright 2015 Drew A. Blessing
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

apache_solr 'tomcat'

apache_solr_instance 'instance1' do
  apache_solr_config 'server' do
    apache_solr_entity 'foo'
  end
  apache_solr_config 'web' do
    apache_solr_entity 'foo'
  end

  apache_solr_service 'instance1'
end
