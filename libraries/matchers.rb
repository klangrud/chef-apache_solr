# Copyright 2015 Drew A. Blessing
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if defined?(ChefSpec)
  ChefSpec.define_matcher :apache_solr
  ChefSpec.define_matcher :apache_solr_config
  ChefSpec.define_matcher :apache_solr_instance
  ChefSpec.define_matcher :apache_solr_service

  def install_apache_solr(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:apache_solr, :install, resource_name)
  end

  def uninstall_apache_solr(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:apache_solr, :uninstall, resource_name)
  end

  def create_apache_solr_config(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:apache_solr_config, :create, resource_name)
  end

  def create_apache_solr_instance(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:apache_solr_instance, :create, resource_name)
  end

  def enable_apache_solr_service(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:apache_solr_service, :enable, resource_name)
  end

  def disable_apache_solr_service(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:apache_solr_service, :disable, resource_name)
  end

  def start_apache_solr_service(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:apache_solr_service, :start, resource_name)
  end

  def stop_apache_solr_service(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:apache_solr_service, :stop, resource_name)
  end

  def restart_apache_solr_service(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:apache_solr_service, :restart, resource_name)
  end
end
